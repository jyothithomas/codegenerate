import React, {useEffect, useState} from 'react'
import './App.css';
import Axios from 'axios'
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/SaveAlt'
import  Typography from '@material-ui/core/Typography';


function App() {
const MAX_NUM_CODE = 1000;
//const id = '';
const [drug_code, setName] = useState('');
const [client_code,setAddress] = useState('');
const [counter,setCount] = useState('');
//const [id,setId] = useState('');
const [UniqueCodelist, setuniqueCodeList] = useState([]);
const [UniqueCodelistDrug, setuniqueCodeListDrug] = useState([]);
const [UniqueCodelistClient, setuniqueCodeListClient] = useState([]);
useEffect(()=> {
  Axios.get('http://localhost:3001/api/get').then((response)=>{
  setuniqueCodeList(response.data)
  });
}, []);
useEffect(()=>{
  Axios.get('http://localhost:3001/api/getdrug').then((response)=> {
    setuniqueCodeListDrug(response.data)
  });
}, []);

useEffect(()=>{
  Axios.get('http://localhost:3001/api/getclient').then((response)=> {
    setuniqueCodeListClient(response.data)
  });
}, []);
const submitDetails = event =>{
  Axios.post("http://localhost:3001/api/insert",{
    drug: drug_code, 
    client: client_code,
    count: counter,
    //id: id
    //unique: UniqueCodelist.drug_code
  }).then(()=>{
    alert("Successfully INSERTed")
  });
  window.location.reload(false);
 // return <Button color="primary">Hello World</Button>
}
/* const updateButton = event => { */
/*   Axios.post("http://localhost:3001/api/update",{ */
/*     unique: unique */
/*   }).then((response)=>{ */
/*     setUniquecode(response.data) */
/*     alert("Updated") */
/*   }); */
/* } */

  return (
    <div>
    <Typography color = "secondary" variant = "h3"> Unique Code Generator</Typography>
    <Typography color = "primary" variant = "h6"> -----For QRcodes-----</Typography>
      <div>
    <label> Drug Code:</label>
        <select value={drug_code}
              onChange={(e) => setName(e.target.value)}> 
      <option value="⬇️ Select a Drug ⬇️"> -- Select a Drug -- </option>
      {UniqueCodelistDrug.map((val) => <option value={val.drug_code}>{val.drug_namel}</option>)}
    </select>
        {/*<input type= "text" name="category_name" onChange ={(e)=> {
          //setuniqueCodeListDrug(e.target.value)
          setName(e.target.value)
        }}/>*/}
        <label> Client Code:</label>
        <select value={client_code}
        onChange={(e) => setAddress(e.target.value)}> 
<option value="⬇️ Select a Client ⬇️"> -- Select a Client -- </option>
{UniqueCodelistClient.map((val) => <option value={val.client_code}>{val.client_name}</option>)}
</select>
<label>No:of Codes to be Generated</label>
<select value = {counter}
onChange={(e) => setCount(e.target.value)}>
  <option value = "Select the Count"> -- Select a Count --</option>
{ Array.from(new Array(MAX_NUM_CODE),(counter, index) => index + 1).map(value => <option key={value} value={value}>{value}</option>) }
            </select>
<Button startIcon = {<SaveIcon />} variant="contained" color = 'secondary' onClick= {submitDetails}> Submit </Button>
        {
          UniqueCodelist.map((val)=>{
           return <h4> {val.unique_code} </h4>
          })
        }     
      </div>
    </div>
  );
}

export default App;
